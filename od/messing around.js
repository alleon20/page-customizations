$(document).ready(function () {
    let isWindowSpinning = false;
    let isWindowShaking = false;
    let isInMirror = false;
    let isUpsideDown = false;
    $('#text-search-field').keyup(e => {
        let val = e.currentTarget.value.toLowerCase().replace(/\s/g, '');
        switch (val) {
            case 'rickrollme':
                window.open('https://www.youtube.com/watch?v=dQw4w9WgXcQ');
                break;
            case 'spintowin':
                if (!isWindowSpinning) {
                    isWindowSpinning = true;
                    $('head').append(`<style id="style-spinner-to-delete">body {animation-name: spin;animation-duration: 5000ms;animation-iteration-count: once;animation-timing-function: linear;} @keyframes spin {from {transform:rotate(0deg);}to {transform:rotate(360deg);}}</style>`);
                    setTimeout(() => {
                        isWindowSpinning = false;
                        $('#style-spinner-to-delete').remove();
                    }, 5500);
                }
                break;
            case 'trollme':
                InformationMessageHelper.ShowInfo('Now deleting entire database. Please hold!');
                setTimeout(() => InformationMessageHelper.ShowSuccess('Database deleted! Reloading...'), 5000);
                setTimeout(() => { location = SiteHelper.SiteRootURL() + 'Error' }, 7500);
                break;
            case 'shakeitbaby':
                if (!isWindowShaking) {
                    isWindowShaking = true;
                    $('head').append(`<style id="style-shaker-to-delete">
                        body {
                            animation-name: shake;
                            animation-duration: 1s;
                            animation-iteration-count: 2;
                        }
                        @keyframes shake {
                           0%, 100% {transform: translateX(0);}
                           10%, 30%, 50%, 70%, 90% {transform: translateX(-10px);}
                           20%, 40%, 60%, 80% {transform: translateX(10px);}
                        }
                    </style>`);
                    setTimeout(() => {
                        isWindowShaking = false;
                        $('#style-shaker-to-delete').remove();
                    }, 10500);
                }
                break;
            case 'fisheye':
                $('head').append(`<style id="style-fisheye-to-delete">*{border-radius: 50% !important;}</style>`);
                setTimeout(() => $('#style-fisheye-to-delete').remove(), 10000);
                break;
            case 'enterthemirrorworld':
                isInMirror = true;
                $('body').css('transform', 'rotateY(180deg)' + (isUpsideDown ? 'rotateX(180deg)' : ''));
                break;
            case 'exitthemirrorworld':
                isInMirror = false;    
                $('body').css('transform', '' + (isUpsideDown ? 'rotateX(180deg)' : ''));
                break;
            case 'upsidedown':
                isUpsideDown = true;
                $('body').css('transform', 'rotateX(180deg)' + (isInMirror ? 'rotateY(180deg)' : ''));
                break;
            case 'downsideup':
                isUpsideDown = false;
                $('body').css('transform', '' + (isInMirror ? 'rotateY(180deg)' : ''));
                break;
            case 'inverted':
            	if (window.sessionStorage.getItem('customize-invert')) {
            		window.sessionStorage.removeItem('customize-invert');
            		$('body').css('filter', '');
            	} else {
	            	window.sessionStorage.setItem('customize-invert', true);
	            	$('body').css('filter', 'invert(1)');
            	}
            	break;

            default: return;
        }
        e.currentTarget.value = '';
    });
    
	$('body').css('filter', window.sessionStorage.getItem('customize-invert') ? 'invert(1)' : '');
});
